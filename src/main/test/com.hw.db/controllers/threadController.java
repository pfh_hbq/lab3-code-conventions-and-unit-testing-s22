package com.hw.db.controllers;

import java.util.*;
import com.hw.db.DAO.*;
import com.hw.db.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;
import org.springframework.*;
import org.springframework.*;
import org.springframework.*;
import org.springframework.*;

import static org.junit.jupiter.api.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

public class threadControllerTests {

	@MockBean
	private final threadController threadController = new threadController();

	private static final MockedStatic<ThreadDAO> THREAD_DAO = mockStatic(ThreadDAO.class);
	private static final MockedStatic<UserDAO> USER_DAO = mockStatic(UserDAO.class);

	@BeforeEach
	void setUp()
	{
		when(ThreadDAO.getThreadById(Mockito.any())).thenReturn(new Thread());
		when(ThreadDAO.getThreadBySlug(Mockito.any())).thenReturn(new Thread());
		User user = new User();
		user.setNickname("n");
		when(UserDAO.Info(Mockito.anyString())).thenReturn(user);}

	@Test
	void checkIdOrSlugTest()
	{
		String param = "slug_example";

		threadController.CheckIdOrSlug(param);
		THREAD_DAO.verify(Mockito.times(1),() -> ThreadDAO.getThreadBySlug(param));}

	@Test
	void checkIdOrSlugIntTest()
	{
		String param = "1";

		threadController.CheckIdOrSlug(param);
		THREAD_DAO.verify(Mockito.times(3),() -> ThreadDAO.getThreadById(Integer.parseInt(param)));
		THREAD_DAO.verifyNoMoreInteractions();}

	@Test
	void createPostTest()
	{
		String slug = "slug";
		List<Post> posts = Collections.emptyList();

		threadController.createPost(slug, posts);
		THREAD_DAO.verify(Mockito.atLeastOnce(),() -> ThreadDAO.createPosts(Mockito.any(), eq(posts), Mockito.anyList()));}

	@Test
	void getPostsTest()
	{
		String slug = "1";
		Integer limit = 1;
		Integer since = 1;
		String sort = "id";
		Boolean desc = false;

		threadController.Posts(slug, limit, since, sort, desc);
		THREAD_DAO.verify(Mockito.times(1),() -> ThreadDAO.getPosts(null, limit, since, sort, desc));}

	@Test
	void changeTest()
	{
		String slug = "1";
		Thread thread = new Thread();
		thread.setId(1);

		when(ThreadDAO.getThreadById(Mockito.any())).thenReturn(thread);
		when(ThreadDAO.getThreadBySlug(Mockito.any())).thenReturn(thread);

		threadController.change(slug, thread);
		THREAD_DAO.verify(Mockito.times(1),() -> ThreadDAO.change(thread, thread));}

	@Test
	void infoTest()
	{
		String slug = "1";

		threadController.info(slug);
		THREAD_DAO.verify(Mockito.times(4),() -> ThreadDAO.getThreadById(Integer.parseInt(slug)));}


	@Test
	void createVote()
	{
		String slug = "slug";
		THREAD_DAO.when(() -> ThreadDAO.getThreadBySlug(slug)).thenThrow(EmptyResultDataAccessException.class);
		assertEquals(HttpStatus.NOT_FOUND, threadController.createVote(slug, mock(Vote.class)).getStatusCode());}
}
